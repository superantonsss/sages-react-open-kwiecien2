import { Playlist } from "../model/Playlist";

type Props = {};
type State = {
  items: Playlist[];
};

export const initialState: State = {
  items: [],
};
/* ===== */

export type Actions = ReturnType<
  typeof PlaylistSelected | typeof PlaylistsRemove | typeof PlaylistsLoaded
>;

export const playlistsReducer = (state = initialState, action: Actions): State => {
  console.log(action)
  switch (action.type) {
    case "PLAYLISTS_SELECT":
      return { ...state, items: state.items.map(p => p.id === action.payload.data.id ? action.payload.data : p) };

    case "PLAYLISTS_LOADED":
      return { ...state, items: action.payload.data };

    case "PLAYLISTS_REMOVE":
      return {
        ...state,
        items: state.items.filter((p) => p.id !== action.payload.id),
      };

    default:
      return state;
  }
};

/* ===== Action Creators ========== */
export const PlaylistSelected = (data: Playlist) =>
({
  type: "PLAYLISTS_SELECT",
  payload: { data },
} as const);

export const PlaylistsRemove = (id: Playlist["id"]) =>
({
  type: "PLAYLISTS_REMOVE",
  payload: { id },
} as const);

export const PlaylistsLoaded = (payload: { data: Playlist[] }) =>
({
  type: "PLAYLISTS_LOADED",
  payload,
} as const);

/*===== State Selectors ============= */

export const playlistSelector = (state: State, selectedId: string | null) =>
  selectedId ? state.items.find(p => p.id === selectedId) : undefined

export default playlistsReducer