import axios from "axios";
import { Playlist } from "../model/Playlist";
import { PagingObject } from "../model/Search";

export const fetchPlaylists = async () => {
  // return new Promise(resolve => resolve(playlistsData))
  // return playlistsData;
  const { data } = await axios.get<PagingObject<Playlist>>("me/playlists");
  return data.items;
};

export const fetchPlaylistById = async (id: string | null) => {
  // return new Promise(resolve => resolve(playlistsData))
  // return playlistsData.find((p) => p.id == id);
  const { data } = await axios.get<Playlist>("playlists/" + id);
  return data;
};

export const playlistsData: Playlist[] = [
  {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "Best playlist",
  },
  {
    id: "234",
    name: "Playlist 234",
    public: false,
    description: "Awesome playlist",
  },
  {
    id: "345",
    name: "Playlist 345",
    public: true,
    description: "Best playlist",
  },
];
