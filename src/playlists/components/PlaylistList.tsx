import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "../../core/helpers/cls";

type Props = {
  playlists: Playlist[];
  selectedId?: string | null;
  onSelect: (id: string) => void;
  onRemove?: (id: string) => void;
};

const PlaylistList = React.memo(
  ({ playlists, selectedId, onSelect, onRemove }: Props) => {
    return (
      <div>
        <div className="list-group">
          {playlists.map((playlist, index) => (
            <div
              key={playlist.id}
              className={cls(
                "list-group-item",
                "list-group-item-action",
                playlist.id === selectedId && "active"
              )}
              onClick={() => onSelect(playlist.id)}
            >
              {index + 1}. {playlist.name}
              {onRemove && (
                <span
                  className="close float-end"
                  onClick={(event) => {
                    // event.preventDefault() // block default brower action (ie. redirect)
                    event.stopPropagation();
                    onRemove(playlist.id);
                  }}
                >
                  &times;
                </span>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  },
  /* props are equal? */
  // (prevProps: Readonly<Props>, nextProps: Readonly<Props>) =>
  //   prevProps.selectedId === nextProps.selectedId &&
  //   prevProps.playlists === nextProps.playlists
);

export default PlaylistList;
