import { useState, useEffect } from "react";

export function useFetch<P, T>(params: P, fetcher: (params: P) => Promise<T>) {
  const [results, setResults] = useState<T | undefined>();
  const [message, setMessage] = useState("");

  useEffect(() => {
    (async () => {
      setResults(undefined);
      setMessage("");
      try {
        setResults(await fetcher(params));
      } catch (error) {
        error instanceof Error && setMessage(error.message);
      }
    })();
  }, [params]);

  return { message, results, isLoading: params && !results && !message };
}