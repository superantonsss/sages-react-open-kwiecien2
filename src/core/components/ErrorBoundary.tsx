import React, { ErrorInfo } from "react";

type State = { hasError: boolean; message: string };

type Props = {
  children: React.ReactNode | React.ReactNode[];
};

export class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false, message: "" };
    // this.setState({
    //   ...this.state,
    //   hasError:true
    // });
  }

  // useEffect:
  // componentDidMount(){}
  // componentDidUpdate(){}
  // componentWillUnmount(){}

  // React.memo:
  // shouldComponentUpdate(){}

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, message: error.message };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    // >>> Sentry, LogRocket, Taillog...
    // this.setState({correctState:123})
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div className="text-center my-5 p-5">
          <h1>Something went wrong.</h1>
          <p>{this.state.message}</p>
          <a href="/">Refresh Page</a>
        </div>
      );
    }

    return this.props.children;
  }
}
