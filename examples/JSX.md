```tsx
interface User {
  id: string;
  name: string;
  color: string;
}

const usersData: User[] = [
  {
    id: "123",
    name: "Alice",
    color: "red",
  },
  {
    id: "234",
    name: "Bob",
    color: "blue",
  },
  {
    id: "345",
    name: "Kate",
    color: "green",
  },
];

type Props = {
  user: User;
};

const UserSection = ({ user }: Props) => {
  const { id, color: userColor, name } = user;

  return (
    <div className="list-group-item" style={{ color: userColor }}>
      <p>{name}</p>
    </div>
  );
  // React.createElement(
  //   "div",
  //   { key, className: "list-group-item", style: { color: userColor } },
  //   React.createElement("p", {}, name)
  // );
};

const UsersList = ({ users }: { users: User[] }) => {
  return (
    <ul className="list-group">
      {users.map((user, index) => (
        <UserSection key={user.id} user={user} />
      ))}
    </ul>
  );

  // return   React.createElement(
  //   "ul",
  //   { className: "list-group" },
  //   users.map((user, index) =>
  //     React.createElement(UserSection, { key: user.id, user })
  //   )
  // );
};

root.render(
  <div className="container my-5">
    <UsersList users={usersData} />
  </div>
);
```

```js
const imie = "Alice";
const zwierze = "dog";

const div = document.createElement("div");
div.innerHTML = `<p >
    ${imie} has a <b>${zwierze}</b> and <input>
</p>`;
document.getElementById("root")?.append(div);
```
