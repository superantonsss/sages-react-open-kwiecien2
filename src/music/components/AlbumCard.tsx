import React from "react";
import { Link } from "react-router-dom";
import { Album } from "../../core/model/Search";

type Props = {
  album: Album;
};

const AlbumCard = ({ album }: Props) => {
  return (
    <div className="card mb-2">
      <img src={album.images[0].url} className="card-img-top" />

      <div className="card-body">
        <h5 className="card-title">
          <Link to={"/music/albums/" + album.id}>{album.name}</Link>
        </h5>
      </div>
    </div>
  );
};

export default AlbumCard;
