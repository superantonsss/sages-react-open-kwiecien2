import axios from "axios";
import { useEffect, useMemo, useState } from "react";
import { useOAuth2Token } from "react-oauth2-hook";
import { isSpotifyError } from "../model/SpotifyError";

axios.defaults.baseURL = "https://api.spotify.com/v1/";

export const useAuth = (): readonly [
  token: string | undefined,
  login: Function,
  logout: Function
] => {
  const [token, getToken, setToken] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    // https://developer.spotify.com/documentation/general/guides/authorization/scopes/
    scope: [""],
    clientID: "45f58671a71243f4aa60d0d07c02a4e8",
    redirectUri: document.location.origin + "/callback",
  });

  useMemo(() => {
    axios.interceptors.response.use(
      (resp) => resp,
      (error: unknown) => {
        console.error(error);
        if (!axios.isAxiosError(error)) {
          throw new Error("Unexpected client error");
        }
        if (error.response?.status === 401) {
          setToken(undefined);
        }
        if (error.response && isSpotifyError(error.response.data)) {
          throw new Error(error.response.data.error.message);
        }
        throw new Error("Unexpected server error");
      }
    );
  }, []);

  useMemo(() => {
    axios.defaults.headers.common = {
      ...axios.defaults.headers.common,
      Authorization: "Bearer " + token,
    };
  }, [token]);

  return [token, getToken, setToken] as const;
};
