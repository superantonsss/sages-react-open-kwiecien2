import { useCallback, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistEditor from "../components/PlaylistEditor";
import PlaylistList from "../components/PlaylistList";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchPlaylistByIdAction,
  fetchPlaylistsAction,
  playlistsRemove,
  selectPlaylistById,
  selectPlaylists,
} from "../../core/reducers/playlistsSlice";
import { RootState } from "../../core/store";
import { fetchPlaylists } from "../../core/services/PlaylistsAPI";
import { PlaylistsLoaded } from "../../core/reducers/playlists";

const PlaylistsReducerView = (props: {}) => {
  // const {getState,dispatch,subscribe} = useStore()
  const items = useSelector(selectPlaylists);
  const message = useSelector((state: RootState) => state.playlists.message);
  const dispatch = useDispatch();

  const [searchParams, setSearchParams] = useSearchParams({ mode: "details" });
  const mode = searchParams.get("mode");
  const selectedId = searchParams.get("id");
  const selected = useSelector(selectPlaylistById(selectedId));

  useEffect(() => {
    // fetchPlaylists().then((data)=>dispatch(PlaylistsLoaded({data})))
    dispatch(fetchPlaylistsAction());
  }, []);

  useEffect(() => {
    selectedId && dispatch(fetchPlaylistByIdAction(selectedId));
  }, [selectedId]);

  const select = useCallback((id: string) => setSearchParams({ id }), []);

  const remove = useCallback(
    (id: string) => dispatch(playlistsRemove({ id })),
    []
  );

  // const save = useCallback(
  //   (id: string): void => dispatch(playlistsRemove(id)),
  //   []
  // );
  if (message)
    return (
      <div className="row">
        {message && <p className="alert alert-danger">{message}</p>}
      </div>
    );

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={items}
            selectedId={selectedId}
            onSelect={select}
            onRemove={remove}
          />
        </div>
        <div className="col">
          {mode === "details" && (
            <PlaylistDetails
              playlist={selected}
              onEdit={() => {
                setSearchParams({ id: selectedId!, mode: "edit" });
              }}
            />
          )}

          {mode === "edit" && (
            <PlaylistEditor
              playlist={selected}
              onCancel={() => {
                setSearchParams({ id: selectedId!, mode: "details" });
              }}
              onSave={() => {}}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default PlaylistsReducerView;
