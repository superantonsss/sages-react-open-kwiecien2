import { useEffect, useState } from "react";
import { Album } from "../model/Search";
import { fetchAlbumSearchResults } from "../services/MusicAPI";

export function useAlbumSearchFetch(query: string) {
  const [results, setResults] = useState<Album[]>([]);
  const [message, setMessage] = useState("");

  useEffect(() => {
    (async () => {
      setResults([]);
      setMessage("");
      if (query) {
        try {
          setResults(await fetchAlbumSearchResults(query));
        } catch (error) {
          error instanceof Error && setMessage(error.message);
        }
      }
    })();
  }, [query]);

  return { message, results };
}
