import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useAuth } from "../hooks/useAuth";
import { UserProfile } from "./UserProfile";

type UserCtx = {
  user: UserProfile | null;
  login: () => void;
  logout: () => void;
} | null;

export const UserContext = React.createContext<UserCtx>(null);

export const UserContextProviver = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [token, getToken, setToken] = useAuth();

  const { data: user = null, remove } = useQuery(
    "user",
    () => axios.get<UserProfile>("me").then((res) => res.data),
    {
      enabled: !!token,
    }
  );

  const login = useCallback(async () => {
    getToken();
  }, []);

  const logout = useCallback(() => {
    setToken(undefined);
    remove();
  }, []);

  const ctx = {
    user,
    login,
    logout,
  };

  return <UserContext.Provider value={ctx}>{children}</UserContext.Provider>;
};
