import React, { FormEvent, useEffect, useRef, useState } from "react";

type Props = {
  query: string;
  onSearch: (query: string) => void;
};

const SearchForm = ({ onSearch, query }: Props) => {
  const [localQuery, setLocalQuery] = useState(query);

  // Update query from parent
  useEffect(() => setLocalQuery(query), [query]);

  // Debounce query changes to parent
  useEffect(() => {
    // Circut-breaker - dont resend to parent!
    if (localQuery === query) return;

    const handle = setTimeout(() => onSearch(localQuery), 500);
    return () => clearTimeout(handle);
  }, [localQuery]);

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    onSearch(localQuery);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={localQuery}
          onChange={(e) => setLocalQuery(e.currentTarget.value)}
        />

        <button className="btn btn-secondary" type="submit">
          Search
        </button>
      </div>
    </form>
  );
};

export default SearchForm;
