import { useContext } from "react";
import { UserContext } from "../contexts/UserContext";


export function useUser() {
  const context = useContext(UserContext);

  if (!context)
    throw new Error("Wrap <UserWidget> inside <UserContextProvider/>");

  return context.user;
}
