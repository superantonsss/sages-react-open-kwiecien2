```js
[1, 2, 3, 4, 5].reduce((acc, x) => {
  console.log(acc, x, acc + x);
  return acc + x;
}, 0);

// 0 1 1
// 1 2 3
// 3 3 6
// 6 4 10
// 10 5 15
15;
```

```js
[1, 2, 3, 4, 5].reduce(
  (state, x) => {
    console.log(state);
    return {
      ...state,
      counter: state.counter + x,
    };
  },
  {
    counter: 0,
    todos: [],
  }
);
```

```js
inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload='kup mleko') => ({type:'ADD_TODO', payload});

[inc(), inc(2), addTodo('placki!'), dec()].reduce( (state, action) => {
  switch(action.type){
      case 'INC':  return {
       ...state, counter: state.counter + action.payload
       };
      case 'DEC':  return {
       ...state, counter: state.counter - action.payload
       };
      case 'ADD_TODO':  return {
       ...state, todos: [ ... state.todos, action.payload]
       };
      default: return state
  }
},{
    counter: 0,
    todos:[]
})
```


```js

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload='kup mleko') => ({type:'ADD_TODO', payload});

state = {
    counter: 0,
    todos:[]
}

reducer = (state, action) => {
  switch(action.type){
      case 'INC':  return {
           ...state, counter: state.counter + action.payload
       };
      case 'DEC':  return {
           ...state, counter: state.counter - action.payload
       };
      case 'ADD_TODO':  return {
           ...state, todos: [ ... state.todos, action.payload]
       };
      default: return state
  }
}

state = reducer(state, inc()); console.log(state);
state = reducer(state, inc(2)); console.log(state);
state = reducer(state, addTodo('placki!')); console.log(state);
state = reducer(state, dec()); console.log(state);


```

```js

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload='kup mleko') => ({type:'ADD_TODO', payload});

state = {
    counter: 0,
    todos:[]
}

counterSlice = (state =0, action) => {
  switch(action.type){
      case 'INC':  return   counter: state.counter + action.payload
      case 'DEC':  return   counter: state.counter - action.payload
      default: return state
  }
}

todosSlice = (state, action) => {
  switch(action.type){ 
      case 'ADD_TODO':  return   [ ... state.todos, action.payload]
      default: return state
  }
}


todos = (state, action) => {
  return {
    ...state,
    counter: counterSlice(state.counter,action),
    todos: todosSlice(state.todos,action),
  }
}

```