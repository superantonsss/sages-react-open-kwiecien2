import React, { useCallback, useEffect, useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistEditor from "../components/PlaylistEditor";
import PlaylistList from "../components/PlaylistList";
import { fetchPlaylists } from "../../core/services/PlaylistsAPI";

declare interface Window {
  crypto: {
    randomUUID(): string;
  };
}

type Props = {};
type ModesEnum = "details" | "editor" | "create";

const PlaylistsView = (props: Props) => {

  const [playlists, setPlaylists] = useState<Playlist[]>([]);
  const [mode, setMode] = useState<ModesEnum>("details");
  const [selectedId, setSelectedId] = useState<string | undefined>("123");
  const [selected, setSelected] = useState<Playlist | undefined>();

  useEffect(() => {
    fetchPlaylists().then((data) => setPlaylists(data));
  }, []);

  const showEdit = useCallback(() => setMode("editor"), []);
  const showDetails = useCallback(() => setMode("details"), []);
  const showCreate = useCallback(() => setMode("create"), []);

  const createPlaylist = useCallback((draft: Playlist) => {
    // draft.id = Math.random().toString(16).slice(2)
    draft.id = window.crypto.randomUUID();
    setPlaylists((playlists) => [...playlists, draft]);
    setSelectedId(draft.id);
    setMode("details");
  }, []);

  const deletePlaylist = useCallback((id: string) => {
    setPlaylists((playlists) => playlists.filter((p) => p.id !== id));
    setSelectedId((selectedId) => (selectedId === id ? undefined : selectedId));
    setMode("details");
  }, []);

  const selectPlaylist = useCallback((id: string) => {
    console.log("selected " + id);
    setSelectedId(id);
  }, []);

  const savePlaylist = useCallback((draft: Playlist) => {
    setPlaylists((playlists) =>
      playlists.map((p) => (p.id === draft.id ? draft : p))
    );
    setMode("details");
  }, []);

  useEffect(() => {
    setSelected(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selectedId}
            onSelect={selectPlaylist}
            onRemove={deletePlaylist}
          />
          <button className="btn btn-info mt-5 float-end" onClick={showCreate}>
            Create Playlist
          </button>
        </div>
        <div className="col">
          {!selected && mode !== "create" && (
            <p className="alert alert-info">No playlist selected</p>
          )}

          {selected && (
            <>
              {mode === "details" && (
                <PlaylistDetails playlist={selected} onEdit={showEdit} />
              )}

              {mode === "editor" && (
                <PlaylistEditor
                  onCancel={showDetails}
                  onSave={savePlaylist}
                  playlist={selected}
                />
              )}
            </>
          )}

          {mode === "create" && (
            <PlaylistEditor onCancel={showDetails} onSave={createPlaylist} />
          )}
        </div>
      </div>
    </div>
  );
};

export default PlaylistsView;
