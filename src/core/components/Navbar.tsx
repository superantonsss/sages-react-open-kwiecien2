import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { cls } from "../helpers/cls";
import { UserWidget } from "./UserWidget";

type Props = {};

const Navbar = (props: Props) => {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="#">
            MusicApp
          </a>
          <button
            className="navbar-toggler"
            type="button"
            onClick={() => setOpen(!open)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className={cls("collapse navbar-collapse", open && "show")}
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink
                  className={({ isActive }) =>
                    "nav-link" + (isActive ? " active placki" : "")
                  }
                  to="/playlists"
                >
                  Playlists
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/music/search">
                  Search
                </NavLink>
              </li>
            </ul>
            <div className="ms-auto navbar-text">
              <UserWidget />
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;


