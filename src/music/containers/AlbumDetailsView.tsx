import React, { useEffect, useRef } from "react";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { Track } from "../../core/model/Search";
import { albumMocks, fetchAlbumById } from "../../core/services/MusicAPI";
import AlbumCard from "../components/AlbumCard";

type Props = {};

const AlbumDetailsView = (props: Props) => {
  const { albumId } = useParams();

  const {
    data: album,
    isLoading,
    error,
  } = useQuery("albumdetails:" + albumId, () => fetchAlbumById(albumId));

  const audioRef = useRef<HTMLAudioElement>(null);

  const playTrack = (track: Track) => {
    if (!audioRef.current) return;

    audioRef.current.src = track.preview_url;
    audioRef.current.volume = 0.2;
    audioRef.current.play();
  };

  if (error instanceof Error)
    return <p className="alert alert-danger">{error.message}</p>;

  if (!album) return <Loading />;

  return (
    <div>
      <div className="row">
        <div className="col mb-3">
          <h1 className="display-3">{album.name}</h1>
          <hr />
          <small className="text-muted">{albumId}</small>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <dl>
            <dt>Name:</dt>
            <dd>{album.name}</dd>
            <dt>Artist</dt>
            <dd>{album.artists[0]?.name}</dd>
            <dt>Release date</dt>
            <dd>{album.release_date}</dd>
            <dt>Tracks</dt>
            <dd>{album.total_tracks}</dd>
          </dl>

          <audio ref={audioRef} className="w-100" controls />

          <div className="list-group">
            {album.tracks?.items.map((track) => (
              <div
                className="list-group-item"
                key={track.id}
                onClick={() => playTrack(track)}
              >
                {track.track_number}. {track.name}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlbumDetailsView;
