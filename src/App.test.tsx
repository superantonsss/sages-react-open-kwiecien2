import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App";

test("renders learn react link", async () => {
  render(
    // <Provider store={fakeStore}>
      <App />
    // </Provider>
  );
  const btn = screen.getByRole("button", { name: "Load Data" });
  userEvent.click(btn);
  await screen.findByText("data loaded");

  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

const calculate = (a: number, b: number) => a + b;

test("", () => {
  expect(calculate(1, 2)).toEqual(3);
  expect(calculate(1, 3)).toEqual(4);
});
